import "../style.css";

// This default export is required in a new `pages/_app.js` file.
export default function ProgrammitApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}
