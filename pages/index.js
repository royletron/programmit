import DefaultLayout from "../layouts/Default";
import Link from "next/link";

export default function HomePage({ urls }) {
  return (
    <DefaultLayout>
      Hello World
      <ul>
        {urls.map((url) => (
          <li key={`url_${url}`}>
            <Link href="/projects/[id]" as={`/projects/${url}`}>
              <a>{url}</a>
            </Link>
          </li>
        ))}
      </ul>
    </DefaultLayout>
  );
}

export async function getStaticProps(context) {
  const fs = require("fs");
  const path = require("path");
  const files = fs.readdirSync(path.join("projects"));
  const urls = files.map((file) => file.replace(".md", ""));
  return {
    props: { urls },
  };
}
