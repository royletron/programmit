import DefaultLayout from "../../layouts/Default";
const ReactMarkdown = require("react-markdown");

export default function ProjectPage({ markdown, frontmatter }) {
  return (
    <DefaultLayout>
      <h1>{frontmatter.title}</h1>
      <ReactMarkdown source={markdown} />
      <a href={frontmatter.link}>Link to project home</a>
    </DefaultLayout>
  );
}

export async function getStaticPaths() {
  const fs = require("fs");
  const path = require("path");
  const files = fs.readdirSync(path.join("projects"));
  const urls = files.map((file) => file.replace(".md", ""));
  return {
    paths: files.map((file) => ({ params: { id: file.replace(".md", "") } })),
    fallback: false, // See the "fallback" section below
  };
}

export async function getStaticProps(context) {
  const fs = require("fs");
  const path = require("path");
  const matter = require("gray-matter");
  const { id } = context.params;
  const data = fs.readFileSync(path.join("projects", `${id}.md`), "utf-8");
  const frontmatter = matter(data);
  return {
    props: { markdown: frontmatter.content, frontmatter: frontmatter.data }, // will be passed to the page component as props
  };
}
