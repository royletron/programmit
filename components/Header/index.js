import styles from "./styles.module.css";
import Link from "next/link";

export default function Header() {
  return (
    <header className={styles.holder}>
      <div className={styles.logo}>
        <Link href="/">
          <a>PROGRAMMIT</a>
        </Link>
      </div>
      <div className={styles.menu}>
        <a>This will be a link</a>
      </div>
    </header>
  );
}
