import styles from "./styles.module.css";
import Header from "../../components/Header";

export default function DefaultLayout({ children }) {
  return (
    <div className={styles.holder}>
      <Header></Header>
      <div className={styles.content}>{children}</div>
    </div>
  );
}
