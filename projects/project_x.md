---
title: Project X
link: https://x.programmit.dev
number: 2
video: https://youtube.com
slides: https://slides.google.com
code: https://gitlab.com/royletron/exp0
---

This is a fake project, just so we know we are not having a fluke.
